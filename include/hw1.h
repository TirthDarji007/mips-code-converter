#ifndef HW_H
#define HW_H

#include "const.h"
#include "instruction.h"
#include <stdbool.h>

//User define functions

// use to compare the string, returns 0 when they are equal. Negate the value if used in if statement.
int stringcmp(char *s, char *t);

// validate hexadesimal number, returns 1 if true, return 0 if false
int validate_hexa(char *aStr);

// validates the base address value and retuns 1 if valid -b is given with valie argument
int validate_flag_b(char *aflag);

//validate the endianness with valid value, 1 is valid, 0 if not valid
int validate_flag_e(char *aflag);

//validate the edian, return 1 if valid
int validate_endian(char *astr);

// get mips opcode
int get_MIPS_opcode(int word);

// get RS field 25:21
int get_MIPS_rs (int word);

// get RT field 20:16
int get_MIPS_rt (int word);

// get RD field 15:11
int get_MIPS_rd (int word);

// get mips immidiate for i type, signed
int get_MIPS_imidiate(int word);

// get RD field 15:11
int get_MIPS_jump (int word);

// get operation code 5:0
int get_MIPS_first5(int word);

// helper to get BCON opcode
Opcode get_BCON_value(int word);

// get the extra value for RTYP  instruction, 10:6
int get_MIPS_EXTRA_RTYP(int word);

// get the extra value for OP_BREAK  instruction, 25:6
int get_MIPS_EXTRA_OP_BREAK(int word);

//it validates if the 4 MSB are same for pc+4 and jump
int validate_jump_address (int word, int jump);

// This appends the opcode
int append_opcode(int word, int opcode);

//ancode helper

// returns the code value of BCON op
int get_BCON_code_ancode(Opcode opcode);

// This appends the special code. 5:0
int append_SPECIAL_5(int word, int opcode);

// append RD field 15:11
int append_MIPS_rd (int word, int rd);

// append RT field 20:16
int append_MIPS_rt (int word, int rt);

// append RD field 25:21
int append_MIPS_rs (int word, int rs);

// append mips immidiate for i type, signed, 15:0
int append_MIPS_imidiate(int word, int immidiate);

//append jump
int append_MIPS_jump (int word, int jump);

// append the extra value for RTYP  instruction, 10:6
int append_MIPS_EXTRA_RTYP(int word, int append);

// append the extra value for OP_BREAK  instruction, 25:6
int append_MIPS_EXTRA_OP_BREAK(int word, int append);

// This method handle the decode option
int handleDecode();

// This function handle the encode option
int handleEncode();

// This function changes the endian
unsigned int endianConvert(unsigned int x);

//This method convert number to binary
int binary_conversion(int num);

// New string compare for syscall
int new_stringcmp(char *s, char *t);

#endif
