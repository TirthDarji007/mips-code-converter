#include "hw1.h"
#include <stdlib.h>
#include <stdio.h>

#ifdef _STRING_H
#error "Do not #include <string.h>. You will get a ZERO."
#endif

#ifdef _STRINGS_H
#error "Do not #include <strings.h>. You will get a ZERO."
#endif

#ifdef _CTYPE_H
#error "Do not #include <ctype.h>. You will get a ZERO."
#endif

/*
 * You may modify this file and/or move the functions contained here
 * to other source files (except for main.c) as you wish.
 */

/**
 * @brief Validates command line arguments passed to the program.
 * @details This function will validate all the arguments passed to the
 * program, returning 1 if validation succeeds and 0 if validation fails.
 * Upon successful return, the selected program options will be set in the
 * global variable "global_options", where they will be accessible
 * elsewhere in the program.
 *
 * @param argc The number of arguments passed to the program from the CLI.
 * @param argv The argument strings passed to the program from the CLI.
 * @return 1 if validation succeeds and 0 if validation fails.
 * Refer to the homework document for the effects of this function on
 * global variables.
 * @modifies global variable "global_options" to contain a bitmap representing
 * the selected options.
 */
int validargs(int argc, char **argv)
{

    //  arguments should be 1 to 7
    if (argc < 2 || argc > 7){
        return 0;
    }

    bool has_h = false;
    bool has_a = false;
    bool has_d = false;
    bool has_b = false;
    bool has_e = false;
    char val_e;
    int val_b;

    // check for flag -h
    if (!stringcmp(*(argv+1),"-h")){
        global_options = 1;
        //printf("flag h check, global: %d\n", EXIT_SUCCESS);
        return 1;
    }

    if (argc > 6){
        // with a or d arguments must be less than or equal to 6
        return 0;
    }

    // check for flag -a and set control signals
    if (!stringcmp(*(argv+1),"-a")){
        //printf("flag a reached\n");
        if (argc == 2){
            //printf("flag a check, no more flag \n");
            has_a = true;
            //global_options = 0;
        }
        // check for -b, -e or nothing
        else if( (argc >= 4) && !stringcmp( *(argv+2), "-b" ) && validate_hexa(*(argv+3) )) {
            // flag -b with valid address
            //printf("base has reached\n");
            // check for e
            if (argc == 4 ){
                //printf("flag a check, only b  \n");
                has_a = true;
                has_b = true;
                val_b = strtol(*(argv+3), NULL, 16);
            }else if( (argc >= 6) && !stringcmp( *(argv+4), "-e" ) && validate_endian(*(argv+5) )) {
                //printf("flag a check, valid b & e  \n");
                has_a = true;
                has_e = true;
                val_e = **(argv+5);
                has_b = true;
                val_b = strtol(*(argv+3), NULL, 16);
            }else{
                //printf("flag a check, valid b & invalid e  \n");
                return 0;
            }
        }
        else if( (argc >= 4) && !stringcmp( *(argv+2), "-e" ) && validate_endian(*(argv+3) )) {
            // endian is given, with valid value
            if (argc == 4 ){
                //printf("flag a check, only e  \n");
                has_a = true;
                has_e = true;
                val_e = **(argv+3);

            }else if( (argc >= 6) && !stringcmp( *(argv+4), "-b" ) && validate_hexa(*(argv+5) )) {
                //printf("flag a check, valid e & b  \n");
                has_a = true;
                has_e = true;
                val_e = **(argv+3);
                has_b = true;
                val_b = strtol(*(argv+5), NULL, 16);
            }else{
                //printf("flag a check, valid e & invalid b  \n");
                return 0;
            }
        }else{
            //printf("flag a check, else uselsess \n");
            return 0;
        }

        //printf("flag a check end \n");
        //return EXIT_SUCCESS;
    }



    // check for flag -d
    if (!stringcmp(*(argv+1),"-d")){
        // flag d is given
        if (argc == 2){
            //printf("flag d check, no more flag \n");
            has_d = true;
            //global_options = 2;
        }
        // check for -b, -e or nothing
        else if( (argc >= 4) && !stringcmp( *(argv+2), "-b" ) && validate_hexa(*(argv+3) )) {
            // flag -b with valid address
            // check for e
            if (argc == 4 ){
                //printf("flag d check, only b  \n");
                has_d = true;
                has_b = true;
                val_b = strtol(*(argv+3), NULL, 16);
            }else if( (argc >= 6) && !stringcmp( *(argv+4), "-e" ) && validate_endian(*(argv+5) )) {
                //printf("flag d check, valid b & e  \n");
                has_d = true;
                has_e = true;
                val_e = **(argv+5);
                has_b = true;
                val_b = strtol(*(argv+3), NULL, 16);
            }else{
                //printf("flag d check, valid b & invalid e  \n");
                return 0;
            }
        }
        else if( (argc >= 4) && !stringcmp( *(argv+2), "-e" ) && validate_endian(*(argv+3) )) {
            // endian is given, with valid value
            if (argc == 4 ){
                //printf("flag d check, only e  \n");
                has_d = true;
                has_e = true;
                val_e = **(argv+3);

            }else if( (argc >= 6) && !stringcmp( *(argv+4), "-b" ) && validate_hexa(*(argv+5) )) {
                //printf("flag d check, valid e & b  \n");
                has_d = true;
                has_e = true;
                val_e = **(argv+3);
                has_b = true;
                val_b = strtol(*(argv+5), NULL, 16);
            }else{
                //printf("flag d check, valid e & invalid b  \n");
                return 0;
            }
        }else{
            //printf("flag d check, else uselsess \n");
            return 0;
        }

        //printf("flag d check, end \n");
        //return EXIT_SUCCESS;
    }

    // all the flags checked and correct
    // has values and actual values are updated
    //printf("value of b: %x \t value of e %c\n",val_b, val_e );

    //set the global value, for only h, only a and only d, values are already set
    if(has_a){
        global_options = 0;
    }
    if (has_d){
        global_options = 2;
    }
    if (has_e){
        if(val_e == 'b'){
            int temp = 4;
            global_options = global_options | temp;
        }
    }
    if(has_b){
        val_b = val_b & 0xFFFFF000;
        global_options = global_options | val_b;
    }
    //printf("global options: %x\n", global_options);

    /* Return Here*/
    return 1;
}

/**
 * @brief Computes the binary code for a MIPS machine instruction.
 * @details This function takes a pointer to an Instruction structure
 * that contains information defining a MIPS machine instruction and
 * computes the binary code for that instruction.  The code is returne
 * in the "value" field of the Instruction structure.
 *
 * @param ip The Instruction structure containing information about the
 * instruction, except for the "value" field.
 * @param addr Address at which the instruction is to appear in memory.
 * The address is used to compute the PC-relative offsets used in branch
 * instructions.
 * @return 1 if the instruction was successfully encoded, 0 otherwise.
 * @modifies the "value" field of the Instruction structure to contain the
 * binary code for the instruction.
 */
int encode(Instruction *ip, unsigned int addr) {

    //int value;         /* The binary instruction word. */
    //Instr_info *info;  /* Info about the instruction type. */
    //char regs[3];      /* Values of the register fields of the instruction word. */, RS, RT, RD
    //int extra;         /* Immediate or other non-register argument decoded from the instruction word. */
    //int args[3];       /* Instruction arguments as in assembly code. */

    int word = 0; // value of decode
    int opcode_value;   // actual value as in int
    Instr_info *info = (*ip).info;
    Type type = (*info).type;

    // check for the endian, if big endian then conver to little, do work, conver back to big
    int endian = global_options >> 2;
    endian = endian & 0x1;
    // if endian 1 means its a big endian
    // If it was big indian then data need to converted to lit endian so prog can work
    if (endian){
        (*ip).regs[0] = endianConvert( (*ip).regs[0] );
        (*ip).regs[1] = endianConvert( (*ip).regs[1] );
        (*ip).regs[2] = endianConvert( (*ip).regs[2] );
        (*ip).extra = endianConvert( (*ip).extra );
        (*ip).args[0] = endianConvert ( (*ip).args[0] );
        (*ip).args[1] = endianConvert ( (*ip).args[1] );
        (*ip).args[2] = endianConvert ( (*ip).args[2] );
    }

    // find opcode
    Opcode opcode = (*info).opcode;
    //printf("opcode is %d\n", opcode);
    //printf("Type is %d\n", type);
    if (opcode == ILLEGL){
        return 0;
    }
    if (opcode==OP_BLTZ || opcode==OP_BGEZ || opcode==OP_BLTZAL || opcode==OP_BGEZAL){
        opcode_value = get_BCON_code_ancode(opcode);
        // bacon value are taken from 20:16
        word = append_MIPS_rt(word, opcode_value);
        word = append_opcode(word, 0x1);
        //printf("Word after BCON check %x \n",word );
    }
    for (int i=0; i<64; i++){
        Opcode sp = specialTable[i];
        if (sp == opcode){
            word =  append_opcode(word, 0);
            word = append_SPECIAL_5(word, i);
            //printf("Word after special check %x \n",word );
        }
    }
    for (int i=2; i<64; i++){
        Opcode op = opcodeTable[i];
        if (op == opcode){
            word =  append_opcode(word, i);
            //printf("Word after opcodetable check %x \n",word );
        }
    }

    //printf("Word after opcode check %x \n",word );

    // decoding by source values
    for (int i = 0; i < 3; i++){
        Source source = (*info).srcs[i];

        if (source == RS){
            word = append_MIPS_rs(word, (*ip).args[i]);
        }
        if (source == RT){
            word = append_MIPS_rt(word, (*ip).args[i]);
        }
        if (source == RD){
            word = append_MIPS_rd(word, (*ip).args[i]);
        }

        if (source == EXTRA){
            // determin the extra field
            if (type == RTYP){
                word = append_MIPS_EXTRA_RTYP(word, (*ip).extra );
            }
            else if (opcode == OP_BREAK){
                word = append_MIPS_EXTRA_OP_BREAK(word, (*ip).extra );
            }
            else if ( opcode>=7 && opcode <= 14){
                //This is a branch ITYP instruction
                int ext = (*ip).extra;
                ext = ext - (addr + 4);
                ext = ext >> 2;
                word = append_MIPS_imidiate(word, ext);
            }
            else if (type == ITYP){
                word = append_MIPS_imidiate(word, (*ip).extra);
            }
            else if ( type == JTYP){
                //printf("jump reached\n");
                int jp = (*ip).extra;
                //int pc = addr;// - 4;
                //jp = jp - pc;
                //jp = jp & 0x0FFFFFFF;
                //printf("JUmp value %x\n", jp);
                int validJp = validate_jump_address(addr, jp);
                //printf("validate jump is %d\n", validJp);
                jp = jp >> 2;
                word = append_MIPS_jump(word, jp);
            }else{
                // error
                // printf("Error in encode source loop for EXTRA");
                return 0;
            }
        }

    }

    //set the encoded value
    (*ip).value = word;

    // If it was big endian then data need to be converted back to big endian
    if (endian){
        (*ip).regs[0] = endianConvert( (*ip).regs[0] );
        (*ip).regs[1] = endianConvert( (*ip).regs[1] );
        (*ip).regs[2] = endianConvert( (*ip).regs[2] );
        (*ip).extra = endianConvert( (*ip).extra );
        (*ip).args[0] = endianConvert ( (*ip).args[0] );
        (*ip).args[1] = endianConvert ( (*ip).args[1] );
        (*ip).args[2] = endianConvert ( (*ip).args[2] );
        (*ip).value = endianConvert ( (*ip).value );
    }

    return 1;
}

/**
 * @brief Decodes the binary code for a MIPS machine instruction.
 * @details This function takes a pointer to an Instruction structure
 * whose "value" field has been initialized to the binary code for
 * MIPS machine instruction and it decodes the instruction to obtain
 * details about the type of instruction and its arguments.
 * The decoded information is returned by setting the other fields
 * of the Instruction structure.
 *
 * @param ip The Instruction structure containing the binary code for
 * a MIPS instruction in its "value" field.
 * @param addr Address at which the instruction appears in memory.
 * The address is used to compute absolute branch addresses from the
 * the PC-relative offsets that occur in the instruction.
 * @return 1 if the instruction was successfully decoded, 0 otherwise.
 * @modifies the fields other than the "value" field to contain the
 * decoded information about the instruction.
 */
int decode(Instruction *ip, unsigned int addr) {

    //int value;         /* The binary instruction word. */
    //Instr_info *info;  /* Info about the instruction type. */
    //char regs[3];      /* Values of the register fields of the instruction word. */, RS, RT, RD
    //int extra;         /* Immediate or other non-register argument decoded from the instruction word. */
    //int args[3];       /* Instruction arguments as in assembly code. */

    int word = (*ip).value;
    Instr_info info;

    // check for the endian, if big endian then conver to little, do work, conver back to big
    int endian = global_options >> 2;
    endian = endian & 0x1;
    // if endian 1 means its a big endian
    if (endian){
        word = endianConvert(word);
    }

    //get the instruction info
    int op = get_MIPS_opcode(word);
    //printf("op: %d\n", op);
    Opcode op_val = opcodeTable[op];
    //printf ("opcode : %d \n", op_val);
    // opcode value find
    if (op_val == SPECIAL){
        op = get_MIPS_first5(word);
        //printf ("special reached, op %d\n", op);
        op_val = specialTable[op];
    }
    if (op_val == BCOND){
        op = get_MIPS_rt(word); // get the value 20:16
        op_val = get_BCON_value(op); // gets the opcode
    }
    if (op_val == ILLEGL){
        return 0;
    }
    //printf ("op val : %d \n", op_val);
    // set info to ip
    info = instrTable[op_val];
    //(*ip).info = &info;
    (*ip).info = &instrTable[op_val];

    //printf("Printing info from decode \nOpcode %d \tType %d ", info.opcode, info.type);

    //get the type of the instructions from instruction info, typedef enum type { NTYP, ITYP, JTYP, RTYP } Type;
    Type type = info.type;
    /*
    //printf("type %d\n", type);
    if (type == RTYP){
        // R type instuction
    }else if(type == ITYP){
        // I type insruction
    }else if (type == JTYP){
        // J type instruction
    }else{
        // Not and instruction
        return 0;
    }
    */

    // setting the regs
    // reset the regs
    //(*ip).regs[0] = get_MIPS_rs(word);  // RS
    //(*ip).regs[1] = get_MIPS_rt(word);  // RT
    //(*ip).regs[2] = get_MIPS_rd(word);  // RD
    // reset the regs
    (*ip).regs[0] = 0;  // RS
    (*ip).regs[1] = 0;  // RT
    (*ip).regs[2] = 0;  // RD
    for(int i = 0; i < 3; i++){
        Source source = info.srcs[i];
        if (source == RS){
            (*ip).regs[0] = get_MIPS_rs(word);
        }
        if (source == RT){
            (*ip).regs[1] = get_MIPS_rt(word);
        }
        if (source == RD){
            (*ip).regs[2] = get_MIPS_rd(word);
        }
    }
    //printf("RS %d \t RT %d \t RD %d\n", (*ip).regs[0], (*ip).regs[1], (*ip).regs[2] );

    //setting up the extra field, branch 7 to 14 inclusive opcodetable, op_value
    for (int i = 0; i < 3 ; i++){
        Source source = info.srcs[i];
        if (source == EXTRA){
            if(type  == RTYP){
                (*ip).extra = get_MIPS_EXTRA_RTYP(word);
                //printf("Extra %d\n",(*ip).extra );
            }
            if ( type == ITYP){
                (*ip).extra = get_MIPS_imidiate(word);
                //printf("Extra %d\n",(*ip).extra );
            }
            if ( op_val == OP_BREAK){
                (*ip).extra = get_MIPS_EXTRA_OP_BREAK(word);
                //printf("Extra %d\n",(*ip).extra );
            }
            if ( op_val >= 7 && op_val <= 14){
                // branch condition
                int temp = get_MIPS_imidiate(word);
                temp = temp << 2;
                temp = temp + addr + 4;
                (*ip).extra = temp;
                //printf("Extra %d\n",(*ip).extra );
            }
            if ( type == JTYP){
                // JTYPE instruction = remain to do .
                int  tp = get_MIPS_jump(word);
                tp = tp << 2;
                int pc4 = addr + 4;
                pc4 = pc4 >> 28;
                pc4 = pc4 << 28;
                tp = tp + pc4;
                int validJump = validate_jump_address( addr, tp);
                if (validJump){
                    (*ip).extra = tp;
                }else{
                    return 0;
                }

                //printf("Extra: %d\n",(*ip).extra );
            }
        }
    }


    // Setting args in  from  the value
    for(int i = 0; i < 3; i++){
        Source source = info.srcs[i];
        if (source == RS){
            (*ip).args[i] = (*ip).regs[0];
        }
        if (source == RT){
            (*ip).args[i] = (*ip).regs[1];
        }
        if (source == RD){
            (*ip).args[i] = (*ip).regs[2];
        }
        if (source == EXTRA){
            (*ip).args[i] = (*ip).extra;
        }
        if (source == NSRC){
            (*ip).args[i] = 0;
        }
    }

    // If it was big indian then data need to converted back to big endian
    if (endian){
        (*ip).regs[0] = endianConvert( (*ip).regs[0] );
        (*ip).regs[1] = endianConvert( (*ip).regs[1] );
        (*ip).regs[2] = endianConvert( (*ip).regs[2] );
        (*ip).extra = endianConvert( (*ip).extra );
        (*ip).args[0] = endianConvert ( (*ip).args[0] );
        (*ip).args[1] = endianConvert ( (*ip).args[1] );
        (*ip).args[2] = endianConvert ( (*ip).args[2] );
    }


    return 1;
}

// This method handle the decode option
int handleDecode(){

    // Handle endian

    //char input[100];
    char *input;
    char filein[4];
    size_t len = 0;
    int val1, val2;
    Instruction ip;
    Instr_info *in;//  = ip.info;
    unsigned int base_addr = global_options & 0xFFFFF000;
    freopen(NULL, "rb", stdin);
    //printf("Base address is: %x\n", base_addr);

    //while ( getline(&input, &len, stdin) != -1 ){

    while ( fread (&(ip.value), sizeof(ip.value), 1, stdin) ){
        //ip.value =   strtol(input, NULL, 16);
        //printf("Ip value is: %x\n", ip.value);
        int dec = decode (&ip, base_addr);
        in = ip.info;
        //printf("Decode return %d\n",dec);
        if (!dec){
            return 0;
        }
        printf( (*in).format  ,ip.args[0], ip.args[1], ip.args[2] );
        printf("\n");
        base_addr = base_addr +4;
    }

    return 1;
}

// This function handle the encode option
int handleEncode(){


    char *input;
    size_t len = 0;
    int count = 0;
    int read_result = 0;
    Instruction ip;
    Instr_info info;//  = ip.info;
    unsigned int base_addr = global_options & 0xFFFFF000;

    freopen(NULL, "wb", stdout);

    while ( getline(&input, &len, stdin) != -1 ){
        //printf("Input is %s\n",input );

        for (int i = 0; i<64; i++){
            //printf("start of loop\n");
            read_result = sscanf(input, instrTable[i].format, &ip.args[0],  &ip.args[1], &ip.args[2] );

            //printf("The read result is %d for i %d\n",read_result, i );
            if (read_result != 0){
                // success
                count = i;
                break;
            }
            if (!new_stringcmp(input,instrTable[i].format)){
                // success
                count = i;
                break;
            }
            if (!stringcmp(input,instrTable[i].format)){
                // success
                count = i;
                break;
            }
        }

        //printf("counter %d\n", count);
        ip.info = &instrTable[count];
        info = instrTable[count];
        ip.regs[0] = 0; //RS
        ip.regs[1] = 0; //RT
        ip.regs[2] = 0; //RD
        ip.extra = 0;
        //printf("First RS %d \t RT %d \t RD %d \t Extra %d\n", ip.regs[0], ip.regs[1], ip.regs[2], ip.extra );

        for (int i =0 ; i<3; i++){
            Source source = info.srcs[i];
            if (source == EXTRA){
                ip.extra = ip.args[i];
            }
            if (source == RS){
                ip.regs[0] = ip.args[i];
            }
            if (source == RT){
                ip.regs[1] = ip.args[i];
            }
            if (source == RD){
                ip.regs[2] = ip.args[i];
            }
        }

        //printf("RS %d \t RT %d \t RD %d \t Extra %d\n", ip.regs[0], ip.regs[1], ip.regs[2], ip.extra );

        int enc = encode (&ip, base_addr);
        //printf("enc val %d\n",enc );
        if (!enc){
            return 0;
        }
        //printf("%x \n", ip.value);
        fwrite( &(ip.value), sizeof(ip.value), 1, stdout);
        count=0;
        base_addr = base_addr +4;
    }
    return 1;
}


// helper functions

// use to compare the string, returns 0 when they are equal. Negate the value if used in if statement.
int stringcmp(char *s, char *t)
{
    //printf("s %d \t t %d, diff %d \n",*s,*t, (*s - *t) );
    for ( ; *s == *t; s++, t++)
        //printf("s %d \t t %d, diff %d \n",*s,*t, (*s - *t) );
    if (*s == '\0' ){
        return 0;
    }
    //printf("s %d \t t %d, diff %d \n",*s,*t, (*s - *t) );
    return *s - *t;
}

// New string compare for syscall
int new_stringcmp(char *s, char *t){

    for ( ; *s == *t; s++, t++){
        //printf("s %s \t\t t %s \n",s,t );
    }
    //printf("Outside Lop s %s \t\t t %s \n",s,t );
    if (*s == '\n' ){
        //printf("s is null\n");
        return 0;
    }
    return 1;
}


// validate hexadesimal number, returns 1 if true, return 0 if false
int validate_hexa(char *aStr)
{
    //printf("valid hexa reached\n");
    char *curr = aStr;
    int count = 0;
    while (*curr != 0)
    {
        if (('A' <= *curr && *curr <= 'F') || ('a' <= *curr && *curr <= 'f') || ('0' <= *curr && *curr <= '9'))
        {
            ++curr;
        }else{
            return 0;
        }
        count++;
    }
    //printf("loop finished\n");
    if (count > 8){
        return 0;
    }
    // multiple of 4096 = last 12 digit must be zero
    int b = (int) strtol(aStr, NULL, 16);
    //printf("value of b %x\n", b);
    int tem = 0x7;
    //tem = tem >> 3;
    //tem = tem << 3;
    b = b & 0x00000FFF;
    //b = b & tem;
    //printf("value of b after move%x\n", b);
    //if (b > 0){
    if (b > 0){
        return 0;
    }
    //printf("returning one\n");
    return 1;
}

//validate the edian, return 1 if valid
int validate_endian(char *astr){
    char c = *astr;
    if ( c == 'l' || c == 'b'){
        return 1;
    }else{
        return 0;
    }
}

// validates the base address value and retuns 1 if valid -b is given with valie argument
int validate_flag_b(char *aflag){
    if (! stringcmp(aflag, "-b")){
        // valid flag given
        if (!*(aflag+1)){
            printf (" b check valid, null hexa ");
        }
        if (validate_hexa( (aflag+1) )){
            // valid hexa  given,
            printf (" b check valid hexa \n");
            return 1;
        }
        else{
            printf (" b check not valid hexa \n");
            return 0;
        }
    }
    return 0;
}

//validate the endianness with valid value, 1 is valid, 0 if not valid
int validate_flag_e(char *aflag){
    if (! stringcmp(aflag, "-e")){
        // valid flag given
        if ( *(aflag+1)== 'l' || *(aflag+1)== 'l' ){
            // valid e value given
            return 1;
        }
        else{
            return 0;
        }
    }
    return 0;
}

// get mips opcode 31:26
int get_MIPS_opcode(int word){
    unsigned int tem = (unsigned int)word >> 26;
    return tem;
}

// get RS field 25:21
int get_MIPS_rs (int word){
    unsigned int temp = word  <<6;
    temp = (unsigned int)temp >> 27;
    return temp;
}

// append RD field 25:21
int append_MIPS_rs (int word, int rs){
    rs = rs << 21;
    word = word | rs;
    return word;
}

// get RT field 20:16
int get_MIPS_rt (int word){
    unsigned int temp = word  <<11;
    temp = (unsigned int)temp >> 27;
    return temp;
}

// append RT field 20:16
int append_MIPS_rt (int word, int rt){
    rt = rt << 16;
    word = word | rt;
    return word;
}

// get RD field 15:11
int get_MIPS_rd (int word){
    unsigned int temp = word  <<16;
    temp = (unsigned int)temp >> 27;
    return temp;
}

// append RD field 15:11
int append_MIPS_rd (int word, int rd){
    rd = rd << 11;
    word = word | rd;
    return word;
}

// get mips immidiate for i type, signed, 15:0
int get_MIPS_imidiate(int word){
    int temp = word  << 16;
    temp = temp >> 16;
    return temp;
}

// append mips immidiate for i type, signed, 15:0
int append_MIPS_imidiate(int word, int immidiate){
    immidiate = immidiate & 0x0000FFFF;
    word = word | immidiate;
    return word;
}

// get field 25:0
int get_MIPS_jump (int word){
    unsigned int temp = word  <<6;
    //int temp = word  <<6;
    temp = (unsigned int)temp >> 6;
    //temp = temp >> 6;
    return temp;
}

// get RD field 25:0
int append_MIPS_jump (int word, int jump){
    word = word | jump;
    return word;
}

// get operation code 5:0
int get_MIPS_first5(int word){
    unsigned int temp = word  <<26;
    temp = (unsigned int)temp >> 26;
    return temp;
}

// get the extra value for RTYP  instruction, 10:6
int get_MIPS_EXTRA_RTYP(int word){
    unsigned int temp = word  <<21;
    temp = (unsigned int)temp >> 27;
    return temp;
}

// append the extra value for RTYP  instruction, 10:6
int append_MIPS_EXTRA_RTYP(int word, int append){
    append = append << 6;
    word = word | append;
    return word;
}

// get the extra value for OP_BREAK  instruction, 25:6
int get_MIPS_EXTRA_OP_BREAK(int word){
    unsigned int temp = word  <<6;
    temp = (unsigned int)temp >> 12;
    return temp;
}

// append the extra value for OP_BREAK  instruction, 25:6
int append_MIPS_EXTRA_OP_BREAK(int word, int append){
    append = append  <<6;
    word = word | append;
    return word;
}

//it validates if the 4 MSB are same for pc+4 and jump
int validate_jump_address (int word, int jump){
    int a = word & 0xF0000000;
    int b = jump & 0xF0000000;
    int c = a - b;
    //printf("a: %x \t b: %x \t c: %x", a, b, c);
    if (c){
        return 0;
    }
    return 1;
}

// helper to get BCON opcode
Opcode get_BCON_value(int word){
    if (word == 0x0){
        return OP_BLTZ;
    }else if (word == 0x1){
        return OP_BGEZ;
    }else if (word == 0x10){
        return OP_BLTZAL;
    }else {
        return OP_BGEZAL;
    }
}

//ancode helper

// returns the code value of BCON op
int get_BCON_code_ancode(Opcode opcode){
    if (opcode == OP_BLTZ){
        return 0x0;
    }
    if (opcode == OP_BGEZ){
        return 0x1;
    }
    if (opcode == OP_BLTZAL){
        return 0x10;
    }
    if ( opcode == OP_BGEZAL){
        return 0x11;
    }
    return 0;
}

// This appends the opcode
int append_opcode(int word, int opcode){
    opcode = opcode << 26;
    word = word | opcode;
    return word;
}

// This appends the special code. 5:0
int append_SPECIAL_5(int word, int opcode){
    // make sure only 5:0 bit exist
    opcode = get_MIPS_first5(opcode);
    word = word | opcode;
    return word;
}

// Little endian to big endian converter
unsigned int endianConvert(unsigned int x){
    return (((x>>24) & 0x000000ff) | ((x>>8) & 0x0000ff00) | ((x<<8) & 0x00ff0000) | ((x<<24) & 0xff000000));
}

//This method convert number to binary
int binary_conversion(int num)
{
    if (num == 0)
    {
        return 0;
    }
    else
    {
        return (num % 2) + 10 * binary_conversion(num / 2);
    }
}